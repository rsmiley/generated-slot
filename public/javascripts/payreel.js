class Payreel {
    constructor(gridSize, reelSize) {
      this.gridSize = gridSize;
      this.reelSize = reelSize;
      this.reels = this.generateReels();
      this.freeSpins = 0;
      this.bonusCount = 0;
    }
  
    generateReels() {
      const symbols = ["symbol1", "symbol2", "symbol3", "symbol4", "symbol5"];
      const reels = [];
  
      for (let i = 0; i < this.gridSize; i++) {
        const reel = [];
  
        for (let j = 0; j < this.reelSize; j++) {
          const randomSymbol = symbols[Math.floor(Math.random() * symbols.length)];
          reel.push(randomSymbol);
        }
  
        reels.push(reel);
      }
  
      return reels;
    }
  
    updateReel(reelIndex) {
      const symbols = ["symbol1", "symbol2", "symbol3", "symbol4", "symbol5"];
  
      for (let i = 0; i < this.reels[reelIndex].length; i++) {
        const randomSymbol = symbols[Math.floor(Math.random() * symbols.length)];
        this.reels[reelIndex][i] = randomSymbol;
      }
  
      this.updateDOM();
    }
  
    updateDOM() {
      for (let i = 0; i < this.gridSize; i++) {
        for (let j = 0; j < this.reelSize; j++) {
          const symbolId = this.reels[i][j];
          const elementId = `symbol-${i}-${j}`;
          const element = document.getElementById(elementId);
  
          if (element) {
            element.setAttribute("src", `/images/${symbolId}.png`);
          }
        }
      }
    }
  
    triggerFreeSpins(count, multiplier) {
      this.freeSpins += count;
      this.multiplier = multiplier;
    }
  
    triggerBonusGame() {
      this.bonusCount++;
    }
  }
  
  module.exports = Payreel;
  