class BonusGame {
    constructor() {
      this.active = false;
      this.bonusSymbols = [];
      this.bonusPrizes = [
        {id: 1, amount: 100},
        {id: 2, amount: 200},
        {id: 3, amount: 500},
        {id: 4, amount: 1000}
      ];
      this.currentPrize = null;
    }
  
    activate(bonusSymbols) {
      this.active = true;
      this.bonusSymbols = bonusSymbols;
      this.currentPrize = null;
    }
  
    deactivate() {
      this.active = false;
      this.bonusSymbols = [];
      this.currentPrize = null;
    }
  
    pickPrize(selectedId) {
      const prize = this.bonusPrizes.find(p => p.id === selectedId);
      this.currentPrize = prize ? prize.amount : 0;
    }
  
    checkForWin(selectedId) {
      return selectedId === this.bonusSymbols[0].id;
    }
  
    getPrizes() {
      return this.bonusPrizes;
    }
  
    getCurrentPrize() {
      return this.currentPrize;
    }
  }
  
  /*
  This BonusGame object has a few properties and methods:

active: A boolean that tracks whether the bonus game is currently active or not.
bonusSymbols: An array of bonus symbols that triggered the bonus game.
bonusPrizes: An array of possible bonus prizes, each with an ID and an amount.
currentPrize: The currently selected bonus prize, or null if none has been selected yet.
activate(): Activates the bonus game and sets the bonusSymbols property to the provided array of bonus symbols.
deactivate(): Deactivates the bonus game and resets all properties to their default values.
pickPrize(selectedId): Sets the currentPrize property to the amount of the selected bonus prize, based on the provided ID.
checkForWin(selectedId): Checks if the provided bonus prize ID matches the ID of the winning bonus symbol (the first one in the bonusSymbols array).
getPrizes(): Returns the array of possible bonus prizes.
getCurrentPrize(): Returns the currently selected bonus prize amount.
*/