const Utils = {
    // Returns a random integer between min (inclusive) and max (inclusive)
    getRandomInt: function(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    // Updates the balance on the screen
    updateBalance: function(balance) {
      const balanceElement = document.querySelector('.balance');
      balanceElement.innerText = `$${balance.toFixed(2)}`;
    },
    // Updates the win on the screen
    updateWin: function(win) {
      const winElement = document.querySelector('.win');
      winElement.innerText = `$${win.toFixed(2)}`;
    },
    // Updates the message on the screen
    updateMessage: function(message) {
      const messageElement = document.querySelector('.message');
      messageElement.innerText = message;
    },
    // Returns an array of random symbols based on the given weightage
    getRandomSymbols: function(weightage) {
      const symbols = [];
      for (let symbol in weightage) {
        const count = weightage[symbol];
        for (let i = 0; i < count; i++) {
          symbols.push(symbol);
        }
      }
      const result = [];
      for (let i = 0; i < 5; i++) {
        result.push(symbols[this.getRandomInt(0, symbols.length - 1)]);
      }
      return result;
    }
  };
  
  module.exports = Utils;
  