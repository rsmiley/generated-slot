// Import modules
import { Payreel } from './payreel.js';
import { BonusGame } from './bonus.js';
import { FreeSpin } from './freespin.js';
import { updateBalance, updateWin, updateMessage } from './utils.js';

// Get elements from DOM
const spinButton = document.querySelector('.spin-btn');
const betAmount = document.querySelector('#bet-amount');
const balanceAmount = document.querySelector('#balance-amount');
const messageDisplay = document.querySelector('.message-display');

// Initialize Payreel object
const payreel = new Payreel(5, 5);

// Initialize BonusGame object
const bonusGame = new BonusGame();

// Initialize FreeSpin object
const freeSpin = new FreeSpin();

// Add event listener to spin button
spinButton.addEventListener('click', () => {
  // Check if bet amount is valid
  const bet = parseInt(betAmount.value);
  if (isNaN(bet) || bet <= 0) {
    updateMessage(messageDisplay, 'Invalid bet amount. Please enter a positive integer.');
    return;
  }

  // Check if balance is sufficient
  const balance = parseInt(balanceAmount.textContent);
  if (balance < bet) {
    updateMessage(messageDisplay, 'Insufficient balance. Please lower your bet.');
    return;
  }

  // Update balance
  const newBalance = balance - bet;
  updateBalance(balanceAmount, newBalance);

  // Spin the reels
  payreel.spin();

  // Check for bonus game trigger
  const bonusTriggered = bonusGame.checkTrigger(payreel.symbols);
  if (bonusTriggered) {
    // Enter bonus game
    const bonusWin = bonusGame.play();
    updateWin(balanceAmount, newBalance + bonusWin);
    updateMessage(messageDisplay, `Congratulations! You won ${bonusWin} in the bonus game.`);
  } else {
    // Check for free spin trigger
    const freeSpinTriggered = freeSpin.checkTrigger(payreel.symbols);
    if (freeSpinTriggered) {
      // Enter free spin mode
      const { count, multiplier } = freeSpin.play();
      updateMessage(messageDisplay, `You won ${count} free spins with a ${multiplier}x win multiplier!`);
    } else {
      // Calculate win amount and update balance
      const winAmount = payreel.calculateWin(bet);
      const totalWin = winAmount * freeSpin.multiplier;
      updateWin(balanceAmount, newBalance + totalWin);

      // Update message
      if (winAmount > 0) {
        updateMessage(messageDisplay, `Congratulations! You won ${totalWin} (${winAmount}x${freeSpin.multiplier})!`);
      } else {
        updateMessage(messageDisplay, 'Sorry, you didn\'t win anything. Try again!');
      }
    }
  }
});
