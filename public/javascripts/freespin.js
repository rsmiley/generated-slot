class FreeSpin {
    constructor() {
      // initialize free spins count and win multiplier
      this.count = 0;
      this.multiplier = 1;
    }
  
    // method to check if free spins are active
    isActive() {
      return this.count > 0;
    }
  
    // method to decrement free spins count
    use() {
      this.count--;
    }
  
    // method to reset free spins count and win multiplier
    reset() {
      this.count = 0;
      this.multiplier = 1;
    }
  
    // method to trigger free spins and set count and multiplier based on number of symbols
    trigger(numSymbols) {
      if (numSymbols === 3) {
        this.count = 5;
        this.multiplier = 1;
      } else if (numSymbols === 4) {
        this.count = 10;
        this.multiplier = 2;
      } else if (numSymbols === 5) {
        this.count = 20;
        this.multiplier = 3;
      }
    }
  }
  
  // create FreeSpin object
  const freeSpin = new FreeSpin();
  